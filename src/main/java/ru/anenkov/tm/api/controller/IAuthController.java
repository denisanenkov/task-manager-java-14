package ru.anenkov.tm.api.controller;

public interface IAuthController {

    void login();

    void logout();

    void register();

    void showProfile();

    void updatePassword();

    void updateUserFirstName();

    void updateUserMiddleName();

    void updateUserLastName();

    void updateUserEmail();

}
