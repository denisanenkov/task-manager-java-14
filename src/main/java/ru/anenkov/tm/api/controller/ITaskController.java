package ru.anenkov.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

    void showTaskByIndex();

    void showTaskByName();

    void showTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void removeTaskById();

    void updateTaskByIndex();

    void updateTaskById();

}
