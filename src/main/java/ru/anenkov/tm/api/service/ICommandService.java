package ru.anenkov.tm.api.service;

import ru.anenkov.tm.dto.Command;

public interface ICommandService {

    String[] getCommands();

    String[] getArgs();

    Command[] getTerminalCommands();

}