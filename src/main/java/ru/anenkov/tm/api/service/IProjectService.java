package ru.anenkov.tm.api.service;

import ru.anenkov.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll(String userId);

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Project project);

    void remove(String userId, Project project);

    void clear(String userId);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project findOneById(String userId, String id);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

    Project removeOneById(String userId, String id);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

}