package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project findOneById(String userId, String id);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

    Project removeOneById(String userId, String id);

}
