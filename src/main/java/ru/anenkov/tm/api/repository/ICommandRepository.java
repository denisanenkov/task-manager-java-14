package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.dto.Command;

public interface ICommandRepository {

    String[] getCommands(Command... values);

    String[] getArgs(Command... values);

    String[] getCommands();

    String[] getArgs();

    Command[] getTerminalCommands();

}
