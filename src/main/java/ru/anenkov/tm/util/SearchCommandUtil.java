package ru.anenkov.tm.util;

import ru.anenkov.tm.exception.system.IncorrectCommandException;
import ru.anenkov.tm.repository.CommandRepository;

public interface SearchCommandUtil {

    static void commandFind(String arg) {
        boolean isFinded = false;
        final CommandRepository commandRepository = new CommandRepository();
        final String[] commands = commandRepository.getCommands();
        for (String command : commands) {
            if (command.equals(arg)) isFinded = true;
        }
        if (isFinded == false) throw new IncorrectCommandException(arg);
    }

}