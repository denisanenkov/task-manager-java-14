package ru.anenkov.tm.controller;

import ru.anenkov.tm.api.controller.IAuthController;
import ru.anenkov.tm.api.service.IAuthService;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.util.TerminalUtil;

public class AuthController implements IAuthController {

    private final IAuthService authService;

    public AuthController(final IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK]");
    }

    @Override
    public void register() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL: ");
        final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        authService.registry(login, password, email);
        System.out.println("[OK]");
    }

    @Override
    public void showProfile() {
        System.out.println("[SHOW USER PROFILE]");
        final User user = authService.showUserProfile();
        System.out.println(user);
        System.out.println("[OK]");
    }

    @Override
    public void updatePassword() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD: ");
        final String newPassword = TerminalUtil.nextLine();
        authService.updatePassword(newPassword);
        System.out.println("[PASSWORD UPDATED]");
        System.out.println("[OK]");
    }

    @Override
    public void updateUserFirstName() {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME: ");
        final String newFirstName = TerminalUtil.nextLine();
        authService.updateUserFirstName(newFirstName);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserMiddleName() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER NEW USER MIDDLE NAME: ");
        final String newMiddleName = TerminalUtil.nextLine();
        authService.updateUserMiddleName(newMiddleName);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserLastName() {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER NEW USER LAST NAME: ");
        final String newLastName = TerminalUtil.nextLine();
        authService.updateUserLastName(newLastName);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserEmail() {
        System.out.println("[UPDATE USER MAIL]");
        System.out.println("ENTER NEW USER MAIL: ");
        final String newUserEmail = TerminalUtil.nextLine();
        authService.updateUserEmail(newUserEmail);
        System.out.println("[OK]");
    }

}
