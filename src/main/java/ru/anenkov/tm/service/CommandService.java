package ru.anenkov.tm.service;

import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.api.service.ICommandService;
import ru.anenkov.tm.dto.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
