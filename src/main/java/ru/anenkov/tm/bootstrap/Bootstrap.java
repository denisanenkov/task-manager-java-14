package ru.anenkov.tm.bootstrap;

import ru.anenkov.tm.api.controller.IAuthController;
import ru.anenkov.tm.api.controller.ICommandController;
import ru.anenkov.tm.api.controller.IProjectController;
import ru.anenkov.tm.api.controller.ITaskController;
import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.constant.ArgumentConst;
import ru.anenkov.tm.constant.MessageConst;
import ru.anenkov.tm.constant.TerminalConst;
import ru.anenkov.tm.controller.AuthController;
import ru.anenkov.tm.controller.CommandController;
import ru.anenkov.tm.controller.ProjectController;
import ru.anenkov.tm.controller.TaskController;
import ru.anenkov.tm.repository.CommandRepository;
import ru.anenkov.tm.repository.ProjectRepository;
import ru.anenkov.tm.repository.TaskRepository;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.*;
import ru.anenkov.tm.util.SearchCommandUtil;
import ru.anenkov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, authService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, authService);

    private final IAuthController authController = new AuthController(authService);

    private void initUsers() {
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        System.out.println(MessageConst.WELCOME);
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        SearchCommandUtil.commandFind(arg);
        switch (arg) {
            case (TerminalConst.HELP):
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.LOGIN:
                authController.login();
                break;
            case TerminalConst.LOGOUT:
                authController.logout();
                break;
            case TerminalConst.REGISTRY:
                authController.register();
                break;
            case TerminalConst.USER_UPDATE_PASSWORD:
                authController.updatePassword();
                break;
            case TerminalConst.USER_SHOW_PROFILE:
                authController.showProfile();
                break;
            case TerminalConst.USER_UPDATE_FIRST_NAME:
                authController.updateUserFirstName();
                break;
            case TerminalConst.USER_UPDATE_MIDDLE_NAME:
                authController.updateUserMiddleName();
                break;
            case TerminalConst.USER_UPDATE_LAST_NAME:
                authController.updateUserLastName();
                break;
            case TerminalConst.USER_UPDATE_EMAIL:
                authController.updateUserEmail();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
        }
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}