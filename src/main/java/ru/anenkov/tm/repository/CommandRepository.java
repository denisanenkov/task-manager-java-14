package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.constant.ArgumentConst;
import ru.anenkov.tm.constant.MessageConst;
import ru.anenkov.tm.constant.TerminalConst;
import ru.anenkov.tm.dto.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, MessageConst.HELP
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, MessageConst.ABOUT
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, MessageConst.VERSION
    );

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, MessageConst.INFO
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null, MessageConst.EXIT
    );

    private static final Command ARGUMENT = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, MessageConst.ARGUMENTS
    );

    private static final Command COMMAND = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, MessageConst.COMMANDS
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, MessageConst.TASK_CREATE
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, MessageConst.TASK_CLEAR
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, MessageConst.TASK_LIST
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, MessageConst.PROJECT_CREATE
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, MessageConst.PROJECT_CLEAR
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, MessageConst.PROJECT_LIST
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, MessageConst.TASK_UPDATE_BY_INDEX
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, MessageConst.TASK_UPDATE_BY_ID
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.TASK_VIEW_BY_ID, null, MessageConst.TASK_VIEW_BY_ID
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.TASK_VIEW_BY_INDEX, null, MessageConst.TASK_VIEW_BY_INDEX
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.TASK_VIEW_BY_NAME, null, MessageConst.TASK_VIEW_BY_NAME
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, MessageConst.TASK_REMOVE_BY_ID
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, MessageConst.TASK_REMOVE_BY_INDEX
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, MessageConst.TASK_REMOVE_BY_NAME
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, MessageConst.PROJECT_UPDATE_BY_INDEX
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, MessageConst.PROJECT_UPDATE_BY_ID
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.PROJECT_VIEW_BY_ID, null, MessageConst.PROJECT_VIEW_BY_ID
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.PROJECT_VIEW_BY_INDEX, null, MessageConst.PROJECT_VIEW_BY_INDEX
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.PROJECT_VIEW_BY_NAME, null, MessageConst.PROJECT_VIEW_BY_NAME
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, MessageConst.PROJECT_REMOVE_BY_ID
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, MessageConst.PROJECT_REMOVE_BY_INDEX
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, MessageConst.PROJECT_REMOVE_BY_NAME
    );

    private static final Command LOGOUT = new Command(
            TerminalConst.LOGOUT, null, MessageConst.LOGOUT
    );

    private static final Command LOGIN = new Command(
            TerminalConst.LOGIN, null, MessageConst.LOGIN
    );

    private static final Command REGISTRY = new Command(
            TerminalConst.REGISTRY, null, MessageConst.REGISTRY
    );

    private static final Command USER_UPDATE_PASSWORD = new Command(
            TerminalConst.USER_UPDATE_PASSWORD, null, MessageConst.USER_UPDATE_PASSWORD
    );

    private static final Command USER_SHOW_PROFILE = new Command(
            TerminalConst.USER_SHOW_PROFILE, null, MessageConst.USER_SHOW_PROFILE
    );

    private static final Command USER_UPDATE_FIRST_NAME = new Command(
            TerminalConst.USER_UPDATE_FIRST_NAME, null, MessageConst.USER_UPDATE_FIRST_NAME
    );

    private static final Command USER_UPDATE_MIDDLE_NAME = new Command(
            TerminalConst.USER_UPDATE_MIDDLE_NAME, null, MessageConst.USER_UPDATE_MIDDLE_NAME
    );

    private static final Command USER_UPDATE_LAST_NAME = new Command(
            TerminalConst.USER_UPDATE_LAST_NAME, null, MessageConst.USER_UPDATE_LAST_NAME
    );

    private static final Command USER_UPDATE_EMAIL = new Command(
            TerminalConst.USER_UPDATE_EMAIL, null, MessageConst.USER_UPDATE_EMAIL
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, ARGUMENT, COMMAND, TASK_CLEAR, TASK_CREATE,
            TASK_LIST, PROJECT_CLEAR, PROJECT_CREATE, PROJECT_LIST, TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID, TASK_VIEW_BY_ID,
            TASK_VIEW_BY_INDEX, TASK_VIEW_BY_NAME, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_REMOVE_BY_NAME, PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID, PROJECT_VIEW_BY_ID,
            PROJECT_VIEW_BY_INDEX, PROJECT_VIEW_BY_NAME, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_REMOVE_BY_NAME, LOGIN, LOGOUT, REGISTRY, USER_SHOW_PROFILE, USER_UPDATE_EMAIL,
            USER_UPDATE_FIRST_NAME, USER_UPDATE_MIDDLE_NAME, USER_UPDATE_LAST_NAME, USER_UPDATE_PASSWORD, EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(final Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (Command current : values) {
            final String name = current.getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(final Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (Command current : values) {
            final String arg = current.getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}