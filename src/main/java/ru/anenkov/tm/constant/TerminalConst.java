package ru.anenkov.tm.constant;

public interface TerminalConst {

    String HELP = "Help";

    String VERSION = "Version";

    String ABOUT = "About";

    String EXIT = "Exit";

    String INFO = "Info";

    String LOGIN = "Login";

    String LOGOUT = "Logout";

    String REGISTRY = "Registry";

    String COMMANDS = "Commands";

    String ARGUMENTS = "Arguments";

    String TASK_LIST = "Task-list";

    String TASK_CLEAR = "Task-clear";

    String TASK_CREATE = "Task-create";

    String PROJECT_LIST = "Project-list";

    String PROJECT_CLEAR = "Project-clear";

    String PROJECT_CREATE = "Project-create";

    String TASK_UPDATE_BY_INDEX = "Task-update-by-index";

    String TASK_UPDATE_BY_ID = "Task-update-by-id";

    String TASK_VIEW_BY_ID = "Task-view-by-id";

    String TASK_VIEW_BY_INDEX = "Task-view-by-index";

    String TASK_VIEW_BY_NAME = "Task-view-by-name";

    String TASK_REMOVE_BY_ID = "Task-remove-by-id";

    String TASK_REMOVE_BY_INDEX = "Task-remove-by-index";

    String TASK_REMOVE_BY_NAME = "Task-remove-by-name";

    String PROJECT_UPDATE_BY_INDEX = "Project-update-by-index";

    String PROJECT_UPDATE_BY_ID = "Project-update-by-id";

    String PROJECT_VIEW_BY_ID = "Project-view-by-id";

    String PROJECT_VIEW_BY_INDEX = "Project-view-by-index";

    String PROJECT_VIEW_BY_NAME = "Project-view-by-name";

    String PROJECT_REMOVE_BY_ID = "Project-remove-by-id";

    String PROJECT_REMOVE_BY_INDEX = "Project-remove-by-index";

    String PROJECT_REMOVE_BY_NAME = "Project-remove-by-name";

    String USER_UPDATE_PASSWORD = "Update-user-password";

    String USER_SHOW_PROFILE = "Show-profile";

    String USER_UPDATE_FIRST_NAME = "Update-first-name";

    String USER_UPDATE_MIDDLE_NAME = "Update-middle-name";

    String USER_UPDATE_LAST_NAME = "Update-last-name";

    String USER_UPDATE_EMAIL = "Update-email";

}